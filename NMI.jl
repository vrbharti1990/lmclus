# This function computes the normalized
# mutual information for a confusion
# matrix c
function getNMI(c)
    normC = c/sum(c);

    rSum = sum(normC, 2);
    cSum = sum(normC, 1);

    NMI = 0;

    for i=1:size(normC,1)
        for j=1:size(normC,2)
            MI = (normC[i,j] * log2(normC[i,j]/(rSum[i]*cSum[j])));

            if ~isnan(MI) 
                NMI = NMI + MI; 
            end 
        end
    end

    log2R = log2(rSum);
    log2C = log2(cSum);

    s1 = -(cSum .* log2C)';
    s2 = -(rSum .* log2R);

    s1[isnan(s1)] = 0;
    s2[isnan(s2)] = 0;

    den = sqrt(sum(s1) * sum(s2));

    NMI = NMI/den;

    return NMI;
end