#include<iostream>
#include<conio.h>

using namespace std;

void printArray(int arr[][5], int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}
}

void main(){
	int i, j, temp, jIndex;

	int a[][5] = { { 4609, 2460, 0, 9, 3920 }, { 0, 0, 3120, 0, 0 }, { 0, 0, 0, 2961, 0 }, { 0, 270, 0, 0, 0 }, { 31, 0, 0, 0, 0 } };

	printArray(a, 5, 5);
	getch();

	for (i = 0; i<5; i++){
		temp = 0;
		for (j = 0; j<5; j++){
			if (a[i][j]>temp){
				temp = a[i][j];
				jIndex = j;
			}
			cout << "\nMax element : " << temp << endl;
			printArray(a, 5, 5);
		}
		for (int k = 0; k < 5; k++){
			a[k][jIndex] = 0;
		}
		for (int k = 0; k<5; k++){
			a[i][k] = 0;
		}
		cout << endl;
	}
	getch();
}