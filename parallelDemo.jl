addprocs(4)

using DistributedArrays
@everywhere using FreqTables
@everywhere using LMCLUS
@everywhere include("NMI.jl")

# call : genPredLab(Ms, length(trueLab))
@everywhere function genPredLab(Ms, l)
    predLab = ones(l);

    for i=1:length(Ms)
        lab = labels(Ms[i]);
        predLab[lab] = i;
    end

    return predLab;
end

# Normalizes (min-max) an array
# each column is taken as a record  
@everywhere function normalizeArr(x)
    for i=1:size(x)[1]
        x[i,:] = (x[i,:]-minimum(x[i,:]))/(maximum(x[i,:])-minimum(x[i,:]));
    end
end

# Assumes 1:(n-1) rows as data and the nth row as label
@everywhere function runLmclus(x, reps)
    trueLab = x[end, :];
    x = x[1:(end-1), :];

    normalizeArr(x);

    params = LMCLUS.Parameters(size(x)[1] - 1);

    nArr = zeros(reps,1);

    for i=1:reps
        Ms = lmclus(x, params);
        predLab = genPredLab(Ms, length(trueLab));
        nArr[i] = getNMI(freqtable(trueLab,predLab));
    end
    # To handle the case when LMCLUS gives just one cluster
    nArr[isnan.(nArr)] = 0;

    return round(mean(nArr),2), round(median(nArr),2), round(std(nArr),2);
end

fileNames = ["d:/file1.csv" "d:/file2.csv" "d:/file3.csv" "d:/file4.csv"]

# Create a distributed array of file fileNames
# such that each file name is mapped to one of 
# the four workers
f = distribute(fileNames);

## cannot pass args
# data = map(CSV.read, f);

# Can pass args
# gives as ouput a distributed array
data = map(f) do x
	readdlm(x, header=false, ',');
end;

tic();
MsArr = map(data) do x
    [runLmclus(x,100)]
end;toc();

show(MsArr)